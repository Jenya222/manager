import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit';

const RouterComponent = () => {
    return (
        <Router>
            <Scene key="root" hideNavBar>
                <Scene key="auth">
                    <Scene
                        key="login"
                        component={LoginForm}
                        title="Please Login" 
                        titleStyle={style.titleStyle}
                        initial
                    />
                </Scene>
                <Scene key="main">
                    <Scene
                        rightTitle="Add"
                        onRight={() => { Actions.employeeCreate() }}
                        key="employeeList"
                        component={EmployeeList}
                        title="Employees" 
                        titleStyle={style.titleStyle}
                    />
                    <Scene
                        key="employeeCreate"
                        component={EmployeeCreate}
                        title="Create Employee" 
                        titleStyle={style.titleStyle}
                    />
                    <Scene
                        key="employeeEdit"
                        component={EmployeeEdit}
                        title="Edit"
                    />
                </Scene>
            </Scene>
        </Router>
    );
};

const style = {
    titleStyle: {
        flex: 1,
        textAlign: 'center'
    }
};

export default RouterComponent;