import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'react-native-firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import LoginForm from './components/LoginForm'
import Router from './Router';

class App extends Component {
  componentWillMount() {
    // Initialize Firebase
    const config = {
      appId: '1:925741807873:android:c6fd0e98f3b016e7',
      apiKey: 'AIzaSyDR6m82aekpN3iuCI04t2Q9HS0v2yrPAtY',
      authDomain: 'manager-66842.firebaseapp.com',
      databaseURL: 'https://manager-66842.firebaseio.com',
      projectId: 'manager-66842',
      storageBucket: 'manager-66842.appspot.com',
      messagingSenderId: '925741807873'
    };

    firebase.initializeApp(config);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
